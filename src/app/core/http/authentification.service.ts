import { Injectable } from '@angular/core';
import {BaseService} from "./base.service";
import {HttpClient} from "@angular/common/http";
import {AppconfigService} from "../helpers/appconfig.service";
import {BehaviorSubject, Observable} from "rxjs";
import {UserAuthentication} from "../models/UserAuthentication";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService extends BaseService{
  public currentUserSubject: BehaviorSubject<UserAuthentication>;
  public currentUser: Observable<UserAuthentication>;
  public resetPasswordObservable = new BehaviorSubject(false);
  constructor(public http: HttpClient, public appConfigService: AppconfigService) {
    super(http, appConfigService);
    // @ts-ignore
    this.currentUserSubject = new BehaviorSubject<UserAuthentication>(JSON.parse(localStorage.getItem('connectedUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): UserAuthentication {
    return this.currentUserSubject.value;
  }
  login(username: string, password: string) {
    return this.create('users/login', {username, password })
        .pipe(map((user: UserAuthentication) => {
          // login successful if there's a jwt token in the response
          if (user && user.token) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('connectedUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
          }
          return user;
        }));
  }
  logout() {
    localStorage.removeItem('connectedUser');
    // @ts-ignore
    this.currentUserSubject.next(null);
    return;
  }

  resetPwdRequest(data : any) {
    return this.create(`users/resetPwd/confirm`, data)
        .pipe(map(data => {
          return data;
        }));
  }
}
