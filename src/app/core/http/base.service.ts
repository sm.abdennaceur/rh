import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AppconfigService} from "../helpers/appconfig.service";

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  apiUrl: any;
  constructor(public http: HttpClient, public appConfigService: AppconfigService) {

  }
  getData(route: string) {
    return this.http.get(this.createCompleteRoute(route, this.appConfigService.configUrl.apiUrl));
  }
  create(route: string, body: any) {
    return this.http.post(this.createCompleteRoute(route, this.appConfigService.configUrl.apiUrl), body);
  }

  update(route: string, body: any) {
    return this.http.put(this.createCompleteRoute(route, this.appConfigService.configUrl.apiUrl), body);
  }

  patch(route: string, body: any){
    return this.http.patch(this.createCompleteRoute(route, this.appConfigService.configUrl.apiUrl), body);
  }

  delete(route: string) {
    return this.http.delete(this.createCompleteRoute(route, this.appConfigService.configUrl.apiUrl));
  }

  public createCompleteRoute = (route: string, envAddress: string) => {
    return `${envAddress}/${route}`;
  }
  public getCompleteRoute = (route: string) => {
    return `${this.appConfigService.configUrl.apiUrl}/${route}`;
  }
}
