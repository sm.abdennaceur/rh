import {Role} from "./role";

export class User {
    id?: string;
    email: string;
    password: string;
    firstName?: string;
    lastName?: string;
    gender?: string;
    roleId?: string;
    role?: Role;
}