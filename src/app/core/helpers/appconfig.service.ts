import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AppconfigService {

  public appConfig: any;
  constructor(private http: HttpClient) { }
  loadAppConfig() {
    return this.http.get('assets/config/config.json')
        .toPromise()
        .then(data => {
          this.appConfig = data;
        });
  }
  get configUrl() {
    return this.appConfig;
  }
}
