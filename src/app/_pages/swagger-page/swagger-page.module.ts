import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwaggerPageRoutingModule } from './swagger-page-routing.module';
import {SwaggerPageComponent} from "./swagger-page.component";


@NgModule({
  declarations: [SwaggerPageComponent],
  exports: [SwaggerPageComponent],
  imports: [
    CommonModule,
    SwaggerPageRoutingModule
  ]
})
export class SwaggerPageModule { }
