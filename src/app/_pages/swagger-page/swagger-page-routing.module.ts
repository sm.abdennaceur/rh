import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SwaggerPageComponent} from "./swagger-page.component";

const routes: Routes = [
  { path: '',
    component: SwaggerPageComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SwaggerPageRoutingModule { }
