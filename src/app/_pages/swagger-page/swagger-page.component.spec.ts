import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwaggerPageComponent } from './swagger-page.component';

describe('SwaggerPageComponent', () => {
  let component: SwaggerPageComponent;
  let fixture: ComponentFixture<SwaggerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwaggerPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwaggerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
