import { AfterViewInit, Component } from '@angular/core';

declare const SwaggerEditorBundle: any;
declare const SwaggerEditorStandalonePreset: any;

@Component({
  selector: 'app-swagger-page',
  templateUrl: './swagger-page.component.html',
  styleUrls: ['./swagger-page.component.scss']
})
export class SwaggerPageComponent implements AfterViewInit {


  constructor() { }
  ngAfterViewInit(): void {

    const editor = SwaggerEditorBundle({
      dom_id: '#swagger-editor',
      layout: 'StandaloneLayout',
      presets: [
        SwaggerEditorStandalonePreset
      ],
      url: 'https://espace-employes.sage.com/rest/swagger.json?noCache=1649772946754'
    });
  }



}
