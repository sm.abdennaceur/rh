import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthentificationService } from "../../core/http/authentification.service";
 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('emailInput') emailInputElment: ElementRef;

  adminLogin: boolean = false;
  loginForm: FormGroup;
  showEror = false;
  show = false;
  error = '';
  returnUrl: string;
  imagePath = environment.imageLogo
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private service: AuthentificationService
    
  ) {}
  ngAfterViewInit(): void {
    this.emailInputElment.nativeElement.focus();
  }

  ngOnInit() {
    this.adminLogin = this.route.snapshot.data.admin;
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/home';
    const currentUser = this.service.currentUserValue;
    if (currentUser) {
      this.router.navigate([this.returnUrl]);
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  toggleShow() {
    this.show = !this.show;
  }

  onSubmit() {

    this.service.login(this.f.email.value, this.f.password.value)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate([this.returnUrl]);
            },
            error => {
              if (error.error.error){
                this.error = error.error.error.message;
              } else{
                this.error = error.error.message;
              }
            });
  }

  
}

