import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Azuneed';

  constructor(
    private translateService: TranslateService,
    @Inject(DOCUMENT) private document: Document){
      
    let lang =  localStorage.getItem('lang')
    lang === null ? this.TranslateLang('en') :  this.TranslateLang(lang)   
  }

  TranslateLang(lang:string){
    let htmlTag = this.document.getElementsByTagName('html')[0] as HTMLHtmlElement;
    htmlTag.dir = lang === 'ar' ? 'rtl' : 'ltr';
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
  }

}
