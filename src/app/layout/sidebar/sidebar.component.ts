import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menu$: Observable<Menu[]>;
  @Output() toggleSideBarEvent: EventEmitter<boolean> = new EventEmitter();
  imageLogo = environment.imageLogo
  constructor() { }

  ngOnInit(): void {
    const menu: Menu[] = [
      {
        title: 'Accueil',
        //description: 'Commandes',
        state: '/home',
        roles: ['MERCHANT', 'SHOP'],
        icon: 'home',
        path: '/home',
        ActiveIcon: 'home',
      },
      {
        title: 'MySpace',
        // description: 'Produits',
        state: '/my-space',
        roles: ['MERCHANT', 'SHOP'],
        icon: 'person',
        path: '/my-space',
        ActiveIcon: 'person',
      },
      {
        title: 'MyCrew',
        // description: 'Statistiques',
        state: '/my-team',
        roles: ['MERCHANT', 'SHOP'],
        icon: 'people',
        path: '/my-team',
        ActiveIcon: 'people',
      },
      {
        title: 'MyCompany',
        // description: 'Boutique',
        state: '/my-company',
        roles: ['MERCHANT', 'SHOP'],
        icon: 'business',
        path: '/my-company',
        ActiveIcon: 'business',
      }
    ];
    this.menu$ = of(menu);
  }
  toggleSideBar() {
    this.toggleSideBarEvent.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }
}
interface Menu {
  title: string;
  //description: string;
  state: string;
  roles?: string[];
  icon: string;
  path: string;
  ActiveIcon?: string;
}