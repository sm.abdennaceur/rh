import { Component, OnInit, Output, EventEmitter, Inject} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { NavigationEnd, Router, ActivatedRoute } from "@angular/router";
import { filter } from "rxjs/operators";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  dropdownPopoverShow = false;
  title: any;
  DarkModechecked = false;
  @Output() toggleLeftBarEvent: EventEmitter<any> = new EventEmitter();
  @Output() toggleSideBarEvent: EventEmitter<any> = new EventEmitter();
  @Output() DarkModeEvent: EventEmitter<boolean> = new EventEmitter();
  languages = [
    { name: 'fr', label: 'Français' },
    { name: 'en', label: 'English' },
    { name: 'ar', label: 'Arabic' }];

  


  constructor(
    private translateService: TranslateService,
    @Inject(DOCUMENT) private document: Document, 
    private router: Router, 
    private route: ActivatedRoute,
    ) { 
      this.title = localStorage.getItem('currentRoute')   
    }

  ngOnInit() {
    this.router.events
    .pipe(filter(event => event instanceof NavigationEnd))
    .subscribe(event => {
      let currentRoute:any = this.route.root;
      do {
        const childrenRoutes = currentRoute.children;
        currentRoute = null;
        childrenRoutes.forEach((route:any) => {
          if (route.outlet === "primary") {
            localStorage.setItem('currentRoute',route.snapshot.data.title)
            this.title = route.snapshot.data.title;
            currentRoute = route;
          }
        });
      } while (currentRoute);
    });
  }
  toggleSideBar() {
    this.toggleSideBarEvent.emit();
    setTimeout(() => { window.dispatchEvent(new Event('resize')) }, 300)
  }
  toggleleftBar() {
    this.toggleLeftBarEvent.emit();
    setTimeout(() => { window.dispatchEvent(new Event('resize')) }, 300)
  }
  changeLanguage(lang: string) {
    localStorage.setItem('lang', lang)
    let htmlTag = this.document.getElementsByTagName('html')[0] as HTMLHtmlElement;
    htmlTag.dir = lang === 'ar' ? 'rtl' : 'ltr';
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
  }
  ModeChanged(){
    this.DarkModeEvent.emit(this.DarkModechecked);
  }
  toggleDropdown(event:any) {
    event.preventDefault();
    if (this.dropdownPopoverShow) {
      this.dropdownPopoverShow = false;
    } else {
      this.dropdownPopoverShow = true;
    }
  }
}
