import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';

@Component({
    selector: 'app-Accueil-page',
    template: `<app-store [ListStories]="stories"></app-store>    
         <app-card-news [ListNews]="news" ></app-card-news>`,
    styles: [``],
})

export class AccueilPage implements OnInit {
    news = [0, 1, 2, 3, 4, 5, 6, 7]
    stories = [
        {
            id: 1,
            description: "HAHAHA",
            has_stories: [
                {
                    id: 1,
                    content: "https://picsum.photos/id/62/900/500"
                },
                {
                    id: 2,
                    content: "https://picsum.photos/id/83/900/500"
                }
            ]
        },
        // {
        //   id: 62,
        //   description: "GG",
        //   has_stories: [
        //     {
        //       id: 3,
        //       content: "https://picsum.photos/id/466/900/500"
        //     },
        //     {
        //       id: 4,
        //       content: "https://picsum.photos/id/982/900/500"
        //     }
        //   ]
        // },
        // {
        //     id: 62,
        //     description: "GG",
        //     has_stories: [
        //       {
        //         id: 3,
        //         content: "https://picsum.photos/id/466/900/500"
        //       },
        //       {
        //         id: 4,
        //         content: "https://picsum.photos/id/982/900/500"
        //       }
        //     ]
        //   }
    ];

    ngOnInit() {

    }

}