import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { AccueilPage } from './page/accueil.page';

const routes: Routes = [
  { path: '', component: AccueilPage,  data: {title: 'Accueil'} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccueilRoutingModule { }
