import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-my-space-page',
    template: `<app-dynamic-nav-menu [ListMenu]="MenuMySpace"></app-dynamic-nav-menu>`,
    styles: [  ],
})

export class MySpacePage implements OnInit {

    MenuMySpace = [{
        label: 'Overview',
        routerlink: ''
      }, {
        label: 'Myfile',
        routerlink: ''
      }, {
        label: 'ProcessingRequests',
        routerlink: ''
      }, {
        label: 'Myrequests',
        routerlink: ''
      }]
      constructor(){}
    ngOnInit(): void {
         
    }

}