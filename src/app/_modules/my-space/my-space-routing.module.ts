import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MySpacePage } from './my-space.page';
 

const routes: Routes = [
  { path: '', component: MySpacePage,  data: {title: 'MySpace'} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MySpaceRoutingModule { }
