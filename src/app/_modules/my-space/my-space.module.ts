import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MySpaceRoutingModule } from './my-space-routing.module';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MySpacePage } from './my-space.page';


@NgModule({
  declarations: [
    MySpacePage
  ],
  imports: [
    CommonModule,
    MySpaceRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class MySpaceModule { }
