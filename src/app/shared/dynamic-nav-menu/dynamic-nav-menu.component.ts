import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic-nav-menu',
  templateUrl: './dynamic-nav-menu.component.html',
  styleUrls: ['./dynamic-nav-menu.component.scss']
})
export class DynamicNavMenuComponent implements OnInit {

  @Input()ListMenu:any;

  constructor() { }

  ngOnInit(): void {
  }

}
