import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicNavMenuComponent } from './dynamic-nav-menu.component';

describe('DynamicNavMenuComponent', () => {
  let component: DynamicNavMenuComponent;
  let fixture: ComponentFixture<DynamicNavMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicNavMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicNavMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
