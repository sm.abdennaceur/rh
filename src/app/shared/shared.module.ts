import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DynamicNavMenuComponent } from './dynamic-nav-menu/dynamic-nav-menu.component';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    DynamicFormComponent,
    DynamicNavMenuComponent
  ],
  imports: [
    CommonModule,
    TranslateModule
  ],
  exports: [DynamicFormComponent, DynamicNavMenuComponent]
})
export class SharedModule { }
