import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { AuthGuard } from "./core/guards/auth.guards";

const loadingPageModule = () => import('./_pages/loading-page/loading-page.module').then(m => m.LoadingPageModule);
const swaggerPageModule = () => import('./_pages/swagger-page/swagger-page.module').then(m => m.SwaggerPageModule);
const loginPageModule = () => import('./_pages/login/login.module').then(m => m.LoginModule);
const AccueilModule = () => import('./_modules/accueil/accueil.module').then(m => m.AccueilModule);
const MySpaceModule = () => import('./_modules/my-space/my-space.module').then(m => m.MySpaceModule);
const MyCrewModule = () => import('./_modules/my-crew/my-crew.module').then(m => m.MyCrewModule);
const MyCompanyModule = () => import('./_modules/my-company/my-company.module').then(m => m.MyCompanyModule);
const AideModule = () => import('./_modules/aide/aide.module').then(m => m.AideModule);

const routes: Routes = [
  {
    path: 'login',
    loadChildren: loginPageModule,
  }, {
    path: 'loading',
    loadChildren: loadingPageModule,
  }, {
    path: 'swagger',
    loadChildren: swaggerPageModule,
    canActivate: [AuthGuard],
  },
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      }, {
        path: 'home',
        loadChildren: AccueilModule,
      },
      {
        path: 'my-space',
        loadChildren: MySpaceModule,
      },
      {
        path: 'my-team',
        loadChildren: MyCrewModule,
      },
      {
        path: 'my-company',
        loadChildren: MyCompanyModule,
      },{
        path: 'aide',
        loadChildren: AideModule,
      }]
  }, { path: '**', redirectTo: 'home', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
